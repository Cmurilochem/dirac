:orphan:

Contributing changes
====================


If you have access to https://gitlab.com/dirac/dirac-private/
-------------------------------------------------------------

Please have a look at :ref:`multiple-remotes`.


Small changes
-------------

For small changes like fixing a typo or fixing small mistakes or relatively
small feature improvements you can fork
https://gitlab.com/dirac/dirac/, create a new branch on your fork, and
directly send a merge request towards the ``master`` branch of
https://gitlab.com/dirac/dirac/.


Larger changes
--------------

For larger changes, anything that would take more than a day of work, we
recommend to first open an issue at https://gitlab.com/dirac/dirac/-/issues and
to describe your idea or your suggestion, before actually spending days or
weeks or months programming it.  It can be very useful to collect feedback
first and let others know.  What you have in mind might already be in the code
or already in the works by somebody else, or there may be a better way to do
it.

Then after you collect feedback, the issue can stay open and you can later
reference it in the merge request weeks or months later and the person
reviewing the merge request will have a context and it will make the review
process easier.

To submit your changes, fork https://gitlab.com/dirac/dirac/, create a new
branch, then later submit merge request towards the ``master`` branch of
https://gitlab.com/dirac/dirac/ and in your commit and/or merge request please
reference the issue where the proposal was discussed.

Another medium to collect feedback before doing a lot of coding is to write to
https://groups.google.com/g/dirac-users.

We are really looking forward to your code changes and improvements!


License terms
-------------

The DIRAC code distributed under the LGPL v2.1 and also all contributions to
the code are understood to be provided under this license with a copyright
shared among the DIRAC code authors. If this is not the case and the submitted
code is provided under a different license and requires a different copyright
notice, please point this out in the merge request.
