See https://github.com/bhelmichparis/laplace-minimax

- The sources correspond to version
  https://github.com/bhelmichparis/laplace-minimax/tree/334620bb7b862e83ec91af0f4bcbd4337c7dc7bc
- Renamed inc/consts.h -> inc/laplace_consts.h for better namespace isolation
