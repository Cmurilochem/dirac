These files are adapted for reading with the C programme basis_set

Philosophy of the dedicated read is to preserve the original format in which the basis set were published.

Required are identifiers :
 - for each element as " ** Elm **" with Elm the element name (max. 3 characters),
   followed by a free format line with the atomic number.
 - for each block of basis functions an identifier $BLOCK
   followed by a free format line with the component (large 0, small 1), the l-value,
   the j-value (l-1/2=0, l+1/2=1), the number of exponents, the number of contracted
  functions.

The exponents and contraction coefficients are read in free format assuming a first (dummy) integer
The order should be (with n the number of contracted functions in the block) :

exponent number, exponent value, contraction coefficient 1, contraction coefficient 2, ..., contraction coefficient n

Additional diffuse or correlating functions are identified by an indentifier followed by a line
with the maximum l-value and number of exponenents of each type (this information is assumed constant for all elements)
The exponents themselves are then on one line started by the element name 

Luuk Visscher, 21-5-2003.

=========================================================================================================
      HOWTO (from LV email, Tue, 25 Apr 2006)

1) Download the original Dyall file, give it an appropriate name and commit this file in the
directory basis/reference_unedited

This step makes sure that we always have the original files available in case there is an error introduced in the
labeling step

2) Make a copy and place this in the reference_edited directory. Do not yet commit.

3) Edit the new file introducing the labels that are required for the c-program. See the README file in
the reference_edited directory.

4) Modify (only small changes should be required) and run the c-programme.

In this step you should carefully check which diffuse/correlation functions are to be included. Since we use
uncontracted basis sets we typically
already have most of the extensions that Ken describes in our standard set. Talk to Andre about this, he should
know what would be a good set-up.

5) Test the new basis set, running atomic calculations comparing with Dyalls reference numbers. Check also for
possible numerical problems due to the
fact that these are nonfamily basis sets.

If this is succesful :

6) Commit the edited file in basis/reference_edited AND also the modified c-programme in utils.

This ensures that we can reconstruct the procedure in case of problems (hence the importance of also commiting the
c-file).

7) Add the new basis (add to an existing file, if it completes the series for other elements). This is probably the
case here.

Test the new basis set file.

8) Commit the modified basis set file.

Please follow this procedure carefully as errors in basis set files are difficult to find and may have a large
impact. The whole thing took me about one or two working days
per basis, some parts are tedious but all in all it is not very difficult to do.



