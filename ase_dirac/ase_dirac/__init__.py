"""
__init__.py file for DIRAC ASE calculator.
"""
from ase_dirac.dirac import DIRAC

__all__ = [
    'DIRAC'
]
