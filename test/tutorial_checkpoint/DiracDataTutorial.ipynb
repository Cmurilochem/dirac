{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "317e6df9",
   "metadata": {},
   "source": [
    "# DIRAC Data\n",
    "\n",
    "*Lucas Visscher, Vrije Universiteit Amsterdam, 2023*\n",
    "\n",
    "Since release DIRAC23 all data suitable for restart and analysis purposes is stored on the file CHECKPOINT.h5\n",
    "\n",
    "This file is in hdf5 format allowing for easy export of the generated data to other formats. The structure is\n",
    "given by the data schema contained in the file `utils/DIRACschema.txt`. This file is processed by a number of Python scripts and then defines the hdf5 directory structure. We will illustrate the extraction of data into Python by means of the included `scf_hf.h5` file that can also be found in the build/test/tutorial_checkpoint directory. It is generated there by the automatic test system or (in case you did not run the test suite) manually by the command `pam --mol=hf.xyz --inp=scf.inp`. Please copy this file to the directory in which you start this Jupyter Notebook before running the next code cell and make sure that you have also installed the h5py library (`conda install h5py` if you are using the conda package manager) in your python environment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ac9cddca",
   "metadata": {},
   "outputs": [],
   "source": [
    "def recursively_load_dict_contents_from_group(h5file, data_dict, path):\n",
    "    \"\"\"\n",
    "    Get a flat dictionary with all data\n",
    "    \"\"\"\n",
    "    import h5py\n",
    "    for key, item in h5file[path].items():\n",
    "        if isinstance(item, h5py._hl.dataset.Dataset):\n",
    "            data_dict[path+key] = item[()]\n",
    "        elif isinstance(item, h5py._hl.group.Group):\n",
    "            recursively_load_dict_contents_from_group(h5file, data_dict, path + key + '/')\n",
    "    return\n",
    "\n",
    "def read_hdf5(file_name):\n",
    "    \"\"\"\n",
    "    Open hdf5-type file and return dictionary of its contents\n",
    "    \"\"\"\n",
    "    import h5py\n",
    "    data_dict = {}\n",
    "    with h5py.File(file_name, 'r') as h5file:\n",
    "        recursively_load_dict_contents_from_group(h5file, data_dict,'/')\n",
    "    return data_dict\n",
    "\n",
    "data = read_hdf5('scf_hf.h5')\n",
    "\n",
    "for key in data:\n",
    "    print ('{}'.format(key,data[key]))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19a75aa1",
   "metadata": {},
   "source": [
    "The above gave a list of all data contained on the file in which the hierarchical structure of the labels is clearly visible. Let's consider printing out the angular momenta of each basis function in both the large and small component AO basis. This is done below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa13a856",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print the large component basis\n",
    "orbmom = data['/input/aobasis/1/orbmom']\n",
    "for shell, lvalue  in enumerate(orbmom):\n",
    "    print ('Shell {} has l-value {}'.format(shell+1,lvalue-1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f71e2f99",
   "metadata": {},
   "source": [
    "Note that Python numbers from zero so we add one to the value of the shell counter to be consistent with how this is also done internally in DIRAC (a Fortran program). On the other hand we see that in DIRAC an orbmom value of 1 indicates an s-function. We therefore need to subtract one from the orbmom numbers to obtain the actual l-value.\n",
    "\n",
    "In the following cell we will list the small component basis together with its expansion centers and exponential parameters after first defining the proper symbols (s, p, d, f, g) to indicate the functions. We also use numpy to reshape the data for ease of printing. In DIRAC all arrays are always stored as 1-dimensional while they are usually conceptually multidimensional."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b674710",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "name_l = ['-','s','p','d','f','g']\n",
    "\n",
    "# Extract the small component basis, this is aobasis #2.\n",
    "n_shells = data['/input/aobasis/2/n_shells'][0]\n",
    "orbmom = data['/input/aobasis/2/orbmom']\n",
    "center = np.array(data['/input/aobasis/2/center']).reshape((n_shells,3))\n",
    "exponents = np.array(data['/input/aobasis/2/exponents'])\n",
    "\n",
    "# Print the small component basis. \n",
    "print ('     Center           Type Exponent\\n',34*'-')\n",
    "for shell, lvalue  in enumerate(orbmom):\n",
    "    print ('{:6.3f} {:6.3f} {:6.3f}    {}  {:8.2e}'.format(*center[shell,0:3],name_l[lvalue],exponents[shell]))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a9ab758",
   "metadata": {},
   "source": [
    "Note that each shell has its own center defined, this is done for simplicity and ease of resorting basis sets. Note also that the coordinates are slightly different from the input values as DIRAC will by default reorient the molecule such that the center of mass is in the origin. This is also visible in the coordinates of the two atoms of this molecule that are updated and can be extracted also from the input section. Note that these are stored in Ångstrom, while the expansion center coordinates are stored in Bohrs (atomic units)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "78f35dd8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print the molecular coordinates\n",
    "print (data['/input/molecule/geometry'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "284132e7",
   "metadata": {},
   "source": [
    "Turning to the results section we will print first the orbital energies obtained in the SCF. These are stored in atomic units (Hartrees in this case) as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eca9f3f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Print the orbital energies\n",
    "print (data['/result/wavefunctions/scf/mobasis/eigenvalues'])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "998e8156",
   "metadata": {},
   "source": [
    "Other data can be extracted in a similar way, please consult file `utils/DIRACschema.txt` for concise definitions of all the data that is stored."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90559201",
   "metadata": {},
   "source": [
    "## Handling old data files\n",
    "\n",
    "It may be useful to be able convert data obtained with an older version of DIRAC to the new hdf5 format. This is possible by running the code cell below where the data directory `dirac21_directory` should point to a directory that contains DFCOEF, MOLECULE.XYZ and AOPROPER files generated by DIRAC21. This will typically be the working directory of such a run or the archive files that were retrieved from it. The script will place a new file CHECKPOINT.h5 in this directory that can be used for analysis and restart purposes. \n",
    "\n",
    "This code assumes DFCOEF files that carry labels (introduced in DIRAC21), if you have older datafiles you should run first the utility program `cf_addlabels` that was distributed with DIRAC21 to add labels to your DFCOEF file.   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1a42ddc1",
   "metadata": {},
   "outputs": [],
   "source": [
    "dirac21_directory = '/Users/you/diracruns/important_old_data'\n",
    "\n",
    "from process_schema import load_diracdata, write_hdf5, data_validity\n",
    "import os\n",
    "chpfile = os.path.join(dirac21_directory,'CHECKPOINT.h5')\n",
    "\n",
    "import h5py\n",
    "# Load the data from the dirac-style files\n",
    "data = load_diracdata(dirac21_directory)\n",
    "# Write and save the checkpoint file after checking its validity\n",
    "if data_validity(data):\n",
    "    write_hdf5(chpfile,data)\n",
    "    print('  Valid hdf5 checkpoint file was constructed from dirac-files')\n",
    "else:\n",
    "    print('  Could not construct hdf5 checkpoint file, dataset is incomplete')\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
