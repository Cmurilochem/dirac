#!/usr/bin/env python

import os
import sys
import shutil

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from runtest_dirac import Filter, TestRun

test = TestRun(__file__, sys.argv)

f = Filter()
f.add(string = '* Electronic eigenvalue',
      rel_tolerance = 1.0e-7)

# run calculation for hydrogen in linear symmetry
test.run(['H.inp'], ['H.mol'], f, '--outcmo')
if os.path.isfile('CHECKPOINT.h5'):
    ext = '.h5'
elif os.path.isfile('CHECKPOINT.noh5.tar.gz'):
    ext = '.noh5.tar.gz'
shutil.move('CHECKPOINT'+ext, 'DFACMO'+ext)

# export coefficients to cs symmetry
test.run(['H_Cs.inp'], ['H_Cs.mol'], f, '--outcmo --put=DFACMO.h5')
shutil.copy('CHECKPOINT'+ext, 'H1'+ext)
shutil.copy('CHECKPOINT'+ext, 'H2'+ext)
shutil.copy('CHECKPOINT'+ext, 'H3'+ext)

# run calculation for nitrogen in linear symmetry
test.run(['N.inp'], ['N.mol'], f, '--outcmo')
shutil.copy('CHECKPOINT'+ext, 'DFACMO'+ext)

# export coefficients to cs symmetry
test.run(['N_Cs.inp'], ['N_Cs.mol'], f, '--outcmo --put=DFACMO.h5')
shutil.copy('CHECKPOINT'+ext, 'N1'+ext)

# run calculation for ammonia; note that it is flat ...
test.run(['NH3.inp'], ['NH3.mol'], f, '--put="N1.h5 H1.h5 H2.h5 H3.h5" ')

# cleanup
os.unlink('CHECKPOINT'+ext)
os.unlink('DFACMO'+ext)
os.unlink('N1'+ext)
os.unlink('H1'+ext)
os.unlink('H2'+ext)
os.unlink('H3'+ext)

### run spinfree version of the test ###

# run calculation for hydrogen in linear symmetry
test.run(['H_sf.inp'], ['H.mol'], f, '--outcmo')
shutil.move('CHECKPOINT'+ext, 'DFACMO'+ext)

# export coefficients to cs symmetry
test.run(['H_Cs_sf.inp'], ['H_Cs.mol'], f, '--outcmo --put=DFACMO.h5')
shutil.copy('CHECKPOINT'+ext, 'H1'+ext)
shutil.copy('CHECKPOINT'+ext, 'H2'+ext)
shutil.copy('CHECKPOINT'+ext, 'H3'+ext)

# run calculation for nitrogen in linear symmetry
test.run(['N_sf.inp'], ['N.mol'], f, '--outcmo')
shutil.copy('CHECKPOINT'+ext, 'DFACMO'+ext)

# export coefficients to cs symmetry
test.run(['N_Cs_sf.inp'], ['N_Cs.mol'], f, '--outcmo --put=DFACMO.h5')
shutil.copy('CHECKPOINT'+ext, 'N1'+ext)

# run calculation for ammonia; note that it is flat ...
test.run(['NH3_sf.inp'], ['NH3.mol'], f, '--put="N1.h5 H1.h5 H2.h5 H3.h5"')

# cleanup
os.unlink('CHECKPOINT'+ext)
os.unlink('DFACMO'+ext)
os.unlink('N1'+ext)
os.unlink('H1'+ext)
os.unlink('H2'+ext)
os.unlink('H3'+ext)

sys.exit(test.return_code)
